-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2019 at 04:49 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `slayerdb_prototype`
--

-- --------------------------------------------------------

--
-- Table structure for table `goal_report`
--

CREATE TABLE `goal_report` (
  `goal_id` int(10) NOT NULL,
  `date` date NOT NULL,
  `value` int(10) NOT NULL,
  `comment` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `goal_report`
--

INSERT INTO `goal_report` (`goal_id`, `date`, `value`, `comment`) VALUES
(231214, '2019-05-31', 1, 'This is a report'),
(231217, '2019-05-31', 1, 'This is a report');

-- --------------------------------------------------------

--
-- Table structure for table `goal_statement`
--

CREATE TABLE `goal_statement` (
  `goal_id` int(10) NOT NULL,
  `slayer_id` int(10) NOT NULL,
  `timespan_id` int(10) NOT NULL,
  `name` varchar(25) NOT NULL,
  `description` varchar(300) NOT NULL,
  `frequency` int(100) NOT NULL,
  `pos_requirement` varchar(100) NOT NULL,
  `neg_requirement` varchar(100) NOT NULL,
  `counts_negative` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `goal_statement`
--

INSERT INTO `goal_statement` (`goal_id`, `slayer_id`, `timespan_id`, `name`, `description`, `frequency`, `pos_requirement`, `neg_requirement`, `counts_negative`) VALUES
(121212, 121212, 121212, 'Excercise', 'Workout', 1, 'workout once', 'workout nil', 0),
(231212, 221212, 221212, 'Write', 'practice', 1, 'workout once', 'workout nil', 0);

-- --------------------------------------------------------

--
-- Table structure for table `guild`
--

CREATE TABLE `guild` (
  `guild_id` int(5) NOT NULL,
  `admin_id` int(8) NOT NULL,
  `guild_name` varchar(25) NOT NULL,
  `guild_description` varchar(300) NOT NULL,
  `allow_negatives` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `guild`
--

INSERT INTO `guild` (`guild_id`, `admin_id`, `guild_name`, `guild_description`, `allow_negatives`) VALUES
(33335, 15555, 'Prime Proto', 'Prototype', 1),
(33355, 15555, 'Prime Proto', 'Prototype', 1);

-- --------------------------------------------------------

--
-- Table structure for table `slayer`
--

CREATE TABLE `slayer` (
  `slayer_id` int(6) NOT NULL,
  `account_id` int(8) NOT NULL,
  `guild_id` int(5) NOT NULL,
  `slayer_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slayer`
--

INSERT INTO `slayer` (`slayer_id`, `account_id`, `guild_id`, `slayer_name`) VALUES
(111111, 22222222, 33333, 'Giles'),
(123436, 12342678, 12145, 'Buffy'),
(123451, 12345671, 12341, 'Xander'),
(131143, 22222223, 33335, 'Korin'),
(131194, 22222223, 33335, 'Korin'),
(131774, 22222223, 33335, 'Korin'),
(131777, 22222223, 33335, 'Korin'),
(131794, 22222223, 33335, 'Korin'),
(191912, 2223341, 44112, 'Psymuelle'),
(191962, 2223341, 44112, 'Psymuelle');

-- --------------------------------------------------------

--
-- Table structure for table `timespan`
--

CREATE TABLE `timespan` (
  `timespan_id` int(10) NOT NULL,
  `guild_id` int(10) NOT NULL,
  `theme_id` int(10) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `span_title` varchar(100) NOT NULL,
  `description` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `timespan`
--

INSERT INTO `timespan` (`timespan_id`, `guild_id`, `theme_id`, `start_date`, `end_date`, `span_title`, `description`) VALUES
(2222, 33335, 11111, '2019-05-01', '2019-05-31', 'Title', 'Description'),
(2222, 33335, 11111, '2019-05-01', '2019-05-31', 'Title', 'Description');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `goal_report`
--
ALTER TABLE `goal_report`
  ADD PRIMARY KEY (`goal_id`);

--
-- Indexes for table `goal_statement`
--
ALTER TABLE `goal_statement`
  ADD PRIMARY KEY (`goal_id`);

--
-- Indexes for table `guild`
--
ALTER TABLE `guild`
  ADD PRIMARY KEY (`guild_id`);

--
-- Indexes for table `slayer`
--
ALTER TABLE `slayer`
  ADD PRIMARY KEY (`slayer_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
